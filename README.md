Делаем образ:
-------------

docker build -t nginx2 .

Запускаем контейнер с конфигом nginx:
-------------------------------------

docker run -d -p 8001:80 -v $(pwd)/nginx.conf:/usr/local/nginx/conf/nginx.conf nginx2
