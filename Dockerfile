FROM debian:9 as build

RUN apt update && apt install wget gcc make libpcre++-dev zlib1g-dev build-essential -y
RUN wget https://nginx.org/download/nginx-1.21.5.tar.gz && tar xfz nginx-1.21.5.tar.gz && cd nginx-1.21.5 && ./configure --with-http_mp4_module && make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
